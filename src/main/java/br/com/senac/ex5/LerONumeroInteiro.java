/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.ex5;

import java.util.Scanner;

/**
 *
 * @author Administrador
 */
public class LerONumeroInteiro {
 
public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int numero;

        System.out.print("Digite um número inteiro: ");
        numero = entrada.nextInt();

        if (numero % 2 == 0) {
            System.out.print("O número informado é par.\n");
        } else {
            System.out.print("O número informado é Impar.\n");
        }
        if (numero > 0) {
            System.out.print("O número informado é positivo.\n");
        } else {
            System.out.print("O número informado é negativo.\n");
        }

    }

    }
